/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.6.24 : Database - bjhome
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bjhome` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bjhome`;

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `address` text NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`user_id`,`username`,`password`,`address`,`email`) values (21,'admin','f6a933a3c2f2d21d776140f4fcf5738e','jogja bay','andri@gmail.com'),(23,'andri','bfface1cb3f9dfd23bd1fa98de267af1','arae','andaricepi@ymail.com');

/*Table structure for table `tblcustomer` */

DROP TABLE IF EXISTS `tblcustomer`;

CREATE TABLE `tblcustomer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `indystry_type` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tblcustomer` */

insert  into `tblcustomer`(`id`,`name`,`city`,`indystry_type`) values (4,'samuel','jakarta','J'),(6,'sony','bandung','J'),(7,'giman','jogja','B'),(9,'dewi','jogja','B');

/*Table structure for table `tblorder` */

DROP TABLE IF EXISTS `tblorder`;

CREATE TABLE `tblorder` (
  `number` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` date DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `salesperson_id` int(11) DEFAULT NULL,
  `amount` int(12) DEFAULT NULL,
  PRIMARY KEY (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `tblorder` */

insert  into `tblorder`(`number`,`order_date`,`cust_id`,`salesperson_id`,`amount`) values (10,'2013-02-08',4,2,540),(20,'2016-01-30',4,8,1800),(30,'2016-09-04',9,1,460),(40,'2016-09-04',7,2,2400),(50,'2016-09-04',6,7,600),(60,'2016-09-04',6,7,720),(70,'2016-09-04',9,7,150);

/*Table structure for table `tblsalesperson` */

DROP TABLE IF EXISTS `tblsalesperson`;

CREATE TABLE `tblsalesperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `salary` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `tblsalesperson` */

insert  into `tblsalesperson`(`id`,`name`,`age`,`salary`) values (1,'joko',61,140000),(2,'ahok',34,44000),(5,'utomo',34,40000),(7,'kevin',41,52000),(8,'johan',57,115000),(11,'anton',38,38000);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
