<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Login Member Area</title>
    <link href="<?php echo base_url(); ?>assetsadmin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-6">
                <h2 class="font-bold">Welcome</h2>
              
                <?php echo $this->session->flashdata('pesan');?>
            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <?php echo form_open(site_url('auth'), 'class=""');?>
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" required="">
                        </div>
                        <?php echo form_submit('submit', 'Login', 'class="btn btn-primary"');?>

                        <a href="#">
                            <small>Forgot password?</small>
                        </a>

                        <p class="text-muted text-center">
                            <small>Do not have an account?</small>
                        </p>
                        
                    <?php echo form_close();?>
                    <p class="m-t">
                        <small>Andri setyabudi &copy; 2016</small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div>
    </div>

</body>

</html>
