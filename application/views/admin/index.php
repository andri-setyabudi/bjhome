<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Data Test | Dashboard</title>
    <link href="<?php echo base_url(); ?>assetsadmin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/css/style.css" rel="stylesheet">

</head>

<body>
    
   
                

    <!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>assetsadmin/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url(); ?>assetsadmin/js/inspinia.js"></script>
    <script src="<?php echo base_url(); ?>assetsadmin/js/plugins/pace/pace.min.js"></script>


    
</body>
</html>
