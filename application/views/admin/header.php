<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <link href="<?php echo base_url(); ?>assetsadmin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assetsadmin/css/style.css" rel="stylesheet">
</head>
<body>
     <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/logo-stickerskins.png" />
                             </span>                         
                        </div>
                    </li>

                    <li class="active">
                        <a href="<?php echo base_url(); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                    </li>  
                    <li class="active">
                        <a href="<?php echo base_url('user'); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Data User</span></a>
                    </li>       

                  </ul>

            </div>
        </nav>
        

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
          
        </div>
            <ul class="nav navbar-top-links navbar-right">
               

                <li>
                    <a href="<?php echo site_url('auth/logout') ?>">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
               
            </ul>

        </nav>

        </div>

</body>
</html>