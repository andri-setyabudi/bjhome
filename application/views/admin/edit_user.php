<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">User</h1>
		</div>
	</div>
	<?=$this->session->flashdata('pesan')?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Edit User</div>
				<?php foreach ($where as $row) { ?>
				<div class="panel-body">
					<div class="row">
						<form method="post" action="<?=base_url(); ?>user/update_user">
						<input type="hidden" name="id_user" value="<?php echo $row->user_id; ?>">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label>Username</label>
								<input class="form-control" placeholder="username" name="username" value="<?php echo $row->username; ?>" required>
							</div>
							<div class="form-group">
								<label>Email</label>
								<input class="form-control" name="email" value="<?php echo $row->email; ?>" >
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<input class="form-control" name="alamat" value="<?php echo $row->address; ?>" >
							</div>
						</div>
						                    <button type="submit" class="btn btn-primary">Simpan</button>
			
                    	</form>
                    	
					
				</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>