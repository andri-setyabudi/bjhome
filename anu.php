RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php?/$1 [L]
<div class="table-responsive">
					<table id="myTable1" class="table table-striped table-bordered">
					<thead>
					<tr>
						<?php foreach ($sa as $sa) { ?>
						<?php } ?>
						<th>
							<div class="panel price panel-red">
								<div class="panel-heading text-center">
									<h5>SIZE S</h5>
								</div>
								<div class="panel-body text-center">
									<p class="lead" style="font-size:20px">
										<strong><?php echo $sa->price; ?></strong>
									</p>
								</div>
							</div>
						</th>
						<?php foreach ($ma as $ma) { ?>
						<?php } ?>
						<th>
							<div class="panel price panel-blue">
								<div class="panel-heading text-center">
									<h5>SIZE M</h5>
								</div>
								<div class="panel-body text-center">
									<p class="lead" style="font-size:20px">
										<strong><?php echo $ma->price; ?></strong>
									</p>
								</div>
							</div>
						</th>
						<?php foreach ($mma as $mma) { ?>
						<?php } ?>
						<th>
							<div class="panel price panel-green">
								<div class="panel-heading text-center">
									<h5>SIZE M +</h5>
								</div>
								<div class="panel-body text-center">
									<p class="lead" style="font-size:20px">
										<strong><?php echo $mma->price; ?></strong>
									</p>
								</div>
							</div>
						</th>
						<?php foreach ($la as $la) { ?>
						<?php } ?>
						<th>
							<div class="panel price panel-red">
								<div class="panel-heading text-center">
									<h5>SIZE L</h5>
								</div>
								<div class="panel-body text-center">
									<p class="lead" style="font-size:20px">
										<strong><?php echo $la->price; ?></strong>
									</p>
								</div>
							</div>
						</th>
						<?php foreach ($xla as $xla) { ?>
						<?php } ?>
						<th>
							<div class="panel price panel-blue">
								<div class="panel-heading text-center">
									<h5>SIZE XL</h5>
								</div>
								<div class="panel-body text-center">
									<p class="lead" style="font-size:20px">
										<strong><?php echo $xla->price; ?></strong>
									</p>
								</div>
							</div>
						</th>
						<?php foreach ($xxla as $xxla) { ?>
						<?php } ?>
						<th>
							<div class="panel price panel-green">
								<div class="panel-heading text-center">
									<h5>SIZE XXL</h5>
								</div>
								<div class="panel-body text-center">
									<p class="lead" style="font-size:20px">
										<strong><?php echo $xxla->price; ?></strong>
									</p>
								</div>
							</div>
						</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							<div class="panel price panel-red">
								<ul class="list-group list-group-flush text-center">
									<?php foreach ($sb as $sb) { ?>
									<li class="list-group-item" style="font-size:12px"><?php echo $sb->brands; ?></li>
									<?php } ?>
								</ul>
							</div>
						</td>
						<td>
							<div class="panel price panel-blue">
								<ul class="list-group list-group-flush text-center">
									<?php foreach ($mb as $mb) { ?>
									<li class="list-group-item" style="font-size:12px"><?php echo $mb->brands; ?></li>
									<?php } ?>
								</ul>
							</div>
						</td>
						<td>
							<div class="panel price panel-green">
								<ul class="list-group list-group-flush text-center">
									<?php foreach ($mmb as $mmb) { ?>
									<li class="list-group-item" style="font-size:12px"><?php echo $mmb->brands; ?></li>
									<?php } ?>
								</ul>
							</div>
						</td>
						<td>
							<div class="panel price panel-red">
								<ul class="list-group list-group-flush text-center">
									<?php foreach ($lb as $lb) { ?>
									<li class="list-group-item" style="font-size:12px"><?php echo $lb->brands; ?></li>
									<?php } ?>
								</ul>
							</div>
						</td>
						<td>
							<div class="panel price panel-blue">
								<ul class="list-group list-group-flush text-center">
									<?php foreach ($xlb as $xlb) { ?>
									<li class="list-group-item" style="font-size:12px"><?php echo $xlb->brands; ?></li>
									<?php } ?>
								</ul>
							</div>
						</td>
						<td>
							<div class="panel price panel-green">
								<ul class="list-group list-group-flush text-center">
									<?php foreach ($xxlb as $xxlb) { ?>
									<li class="list-group-item" style="font-size:12px"><?php echo $xxlb->brands; ?></li>
									<?php } ?>
								</ul>
							</div>
						</td>
					</tr>
					</tbody>
					</table>
				</div>